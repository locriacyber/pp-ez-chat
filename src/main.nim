import pp_ez_chat/ui
import pkg/ui as libui

import std/[net, strutils, strformat, os, json, monotimes]

type UiMessageType = enum
  SendMessage

type UiMessage = object
  case t: UiMessageType
  of SendMessage:
    message: string

var chan_sync: Channel[UiMessage]

const request_payload = """{"type":"recv_all","args":null}"""

{.push header: "socket-impl.c".}
type NativeSocket = distinct cint
proc unix_socket_open(path: cstring): NativeSocket
  {.importc: "unix_socket_open_bind".}
proc unix_socket_sendto(s: NativeSocket, path: cstring, data: ptr openArray[uint8], len: csize_t): csize_t
  {.importc: "unix_socket_sendto".}
proc unix_socket_recvfrom(s: NativeSocket, path: ptr cstring, data: ptr openArray[uint8], maxlen: csize_t): csize_t
  {.importc: "unix_socket_recvfrom".}
{.pop.}

proc toString(bytes: openarray[byte]): string =
  result = newString(bytes.len)
  copyMem(result[0].addr, bytes[0].unsafeAddr, bytes.len)

proc poll_for_message(opts: (ref UiData, string)) =
  let (vud, peer) = opts
  let time = getMonoTime()
  # let ip = "127.0.0.3"
  # let port = 2022.Port
  # let sock = newSocket(net.AF_INET.c_int, net.SOCK_DGRAM.c_int, 0)
  let sock = unix_socket_open(fmt"/tmp/test-sock-{time}".cstring)
  while true:
    let frame_id = "-"
    let send_data = (frame_id & "\0" & request_payload)
    template send_bytes: untyped = send_data.toOpenArrayByte(0, send_data.len)
    let len_sent = unix_socket_sendto(sock, peer.cstring, send_bytes.addr, send_bytes.len.csize_t)
    assert len_sent.int == send_bytes.len
    var
      data_buffer: array[1024*1024, uint8]
      peeraddr: cstring
    let len_recv = unix_socket_recvfrom(sock, peeraddr.addr, data_buffer.openArray.addr, data_buffer.len.csize_t)
    # assert len_sent.int == send_bytes.len
    let data = data_buffer[0..len_recv].toString
    echo data
    let s = data.split('\0')
    assert s[0] == frame_id
    let jsondata = parseJson(s[1])
    for pp_event in jsondata.getElems:
      let args = pp_event.getElems
      let arg1 = args[1]
      case args[0].getStr:
        of "chat.user.message":
          let baked_message = fmt"""{arg1["username"].getStr}: {arg1["message"].getStr}"""
          chan_sync.send(UiMessage(t: SendMessage, message:baked_message))
    os.sleep(1000)

proc task_ui(ud: ref UiData) = 
  ui_init(ud)
  defer: libui.uninit()
    # ui_add_message(vud, baked_message)
  proc poll(ud: ref UiData) {.cdecl, gcsafe.} =
    defer: libui.queueMain(poll, ud)
    let (hasdata, data) = chan_sync.tryrecv()
    if hasdata:
      case data.t:
        of Sendmessage:
          ui_add_message(ud, data.message)
  
  libui.queueMain(poll, ud)
  libui.main()
  echo "UI exited"
  # libui.mainLoop()

when isMainModule:
  var ud: ref UiData = new(UiData)
  chan_sync.open()
  var thr_ui: Thread[ref UiData]
  var thr_socket: Thread[(ref UiData, string)]
  createThread(thr_ui, task_ui, ud)
  createThread(thr_socket, poll_for_message, (ud, "/home/user/.local/share/ppase-ng/rpc"))
  joinThread(thr_ui)
  # joinThread(thr_socket)
