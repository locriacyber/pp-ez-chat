import pkg/ui
import pkg/ui/rawui

type Message = object
  content: string

proc clear(box: ui.Box) =
  while box.children.len > 0:
    box.delete(0)

var example_messages = @[
  Message(content:"Hello"),
  Message(content:"hi"),
  Message(content:"what"),
]

type UiData* = object
  group_messages: ui.Group
  box_messages: ui.Widget # Child of group_messages. destroy on update
  messages: seq[Message]

using ud: UiData
using vud: ref UiData

proc ui_refresh_messages(vud) =
  let parent = ui.newVerticalBox()
  for m in vud.messages:
    let box = ui.newHorizontalBox()
    box.padded = true
    let lbl_msg = ui.newLabel(m.content)
    box.add lbl_msg
    parent.add box
  vud.group_messages.child = parent
  if vud.box_messages != nil:
    rawui.controlDestroy(vud.box_messages.impl)
  vud.box_messages = parent

proc ui_add_message*(vud; msg: string) =
  vud.messages.add Message(content:msg)
  ui_refresh_messages(vud)

proc ui_message_display(vud): auto =
  result = ui.newGroup("Messages")
  vud.group_messages = result
  ui_refresh_messages(vud)

proc ui_info_display(vud): auto =
  # result = ui
  # result = ui.newGroup("")
  # result.
  # result.margined = true
  # result.child = ui_message_display()
  result = ui_message_display(vud)

proc ui_controls(vud): auto =
  result = ui.newGroup("Actions")

  let box = ui.newHorizontalBox()
  box.padded = true

  let text_entry = ui.newEntry("")
  let btn_send = ui.newButton("Send")
  btn_send.onclick = proc = ui_add_message(vud, text_entry.text)
  
  box.add text_entry, true
  box.add btn_send

  result.child = box

proc ui_root(vud): auto =
  result = ui.newVerticalBox()
  result.add ui_info_display(vud), true
  result.add ui_controls(vud)

proc ui_init*(vud) =
  ui.init()
  let win = ui.newWindow("Hello", 250, 500, false)
  win.child = ui_root(vud)
  win.margined = true

  proc on_close: bool =
    win.destroy()
    ui.quit()

  win.onclosing = on_close
  ui.show win

when isMainModule:
  let vud = new(UiData)
  vud.messages = example_messages
  ui_init()
  ui.mainLoop()
