import std/[net, strutils, strformat]

let ip = "127.0.0.3"
let port = 2022.Port
let sock = newSocket(net.AF_INET.c_int, net.SOCK_DGRAM.c_int, 0)

const message = """[["chat.user.message",{"username":"KILLAKING","color":0,"guild":"","message":"the Kaiser lives eternally","admin":false,"mod":false,"chatmod":false,"premium":false,"icons":[],"rainbow":false,"xmas":false,"channel":"global","mention":"","target":"","type":"chat","createdAt":"2022-03-26T21:50:44Z","silent":true}]]"""

sock.bindAddr(port, ip)
echo fmt"Listening on {ip}:{port}"

while true:
  var
    data: string
    peeraddr: string
    peerport: Port
  discard sock.recvFrom(data, 1024, peeraddr, peerport)
  let s = data.split("\0", maxSplit=2)
  echo fmt"Received data: {s[1]}"
  let id = s[0]
  # let message = ""  
  sock.sendTo(peeraddr, peerport, id & "\0" & message)
