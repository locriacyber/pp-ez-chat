#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>


void unix_socket_set_addr(struct sockaddr_un* addr, const char* path) {
    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr->sun_family = AF_UNIX;
    strncpy(addr->sun_path, path, sizeof(addr->sun_path) - 1);
}

/**
 * Success when Return code > 0
 * otherwise idk what error that means
**/
int unix_socket_open_bind(char* path) {
    int sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sock < 0) return sock;
    struct sockaddr_un addr;
    unix_socket_set_addr(&addr, path);
    int err = bind(sock, &addr, sizeof(addr));
    if (err < 0) return err;
    return sock;
}

size_t unix_socket_sendto(int sock, char* path, uint8_t* data, size_t len) {
    struct sockaddr_un addr;
    unix_socket_set_addr(&addr, path);
    return sendto(sock, data, len, 0, &addr, sizeof(addr));
}

size_t unix_socket_recvfrom(int sock, char** path, uint8_t* data, size_t maxlen) {
    struct sockaddr_un addr;
    socklen_t addrlen;
    size_t ret = recvfrom(sock, data, maxlen, 0, &addr, &addrlen);
    typeof(addr.sun_path)* path_buffer = malloc(sizeof(addr.sun_path));
    if (path_buffer == 0) return 0;
    strncpy(*path_buffer, addr.sun_path, sizeof(addr.sun_path));
    *path = path_buffer;
    return ret;
}
