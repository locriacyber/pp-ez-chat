"""Paint a square"""

import sys
import socket
import os
import time
import json
import atexit

rpc_sock_path = sys.argv[1]
sock_path = os.path.realpath("sock")

def remove_sock():
    try:
        os.remove(sock_path)
    except FileNotFoundError:
        pass

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM, 0)
atexit.register(remove_sock)
remove_sock()
sock.bind(sock_path)
sock.connect(rpc_sock_path)
colors = [0, 5]
color_id = 0
while True:
    for y in range(1839, 1849):
        for x in (1439, 1450):
            payload = {
                "type": "send_one",
                "args":["p", [x, y, colors[color_id], 1]],
            }
            sock.send((f"{x},{y}\0" + json.dumps(payload)).encode())
            time.sleep(0.02)
    color_id = (color_id+1) % len(colors)
