"""Simple UNIX datagram echo server"""

import sys
import socket
import os
import time
import json
import atexit

sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM, 0)
sock.bind("/tmp/b")

while True:
    data, addr = sock.recvfrom(1000)
    print(data, addr)
    sock.sendto(data, addr)
